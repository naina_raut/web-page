<!--Name:Naina Raut 
	Date:11/20/2015
	Purpose:to build a web page for a commercial website
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mock Web Page</title>

<!-- Standard .css and .js links  -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Custom .css link -->
<link rel="stylesheet" type="text/css" href="HomePageCSS.css">

<!-- script code for display of sub menu on mouse hover -->
<script type="text/javascript">
jQuery(document).ready(function($)
{   
	$('a','.nav-tabs > li').hover(function(){ 
		$(this).trigger('click'); 
		});
		
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$('.tab-pane').not($(this).attr('href')).removeClass('active');
		});
});
</script> 

</head>
<body>

<!--To display the main heading and logo-->
<table width="100%">
	<tr>
	    <td align="left" width="30%">
			<img src="logo.png">
		</td>
		<td>
			<h1 style="color:GREEN ; font-size:50px;font-family:arial"><b>Green World Fashion</b></h1>
		</td>
	</tr>	
</table>

<!--to display the menu and sub menu -->
<div class="container">

  <!-- main menu section -->
  <div class="row">
    <ul class="nav nav-tabs pull-right" id="myTab">
      <li class="active">
      	<form>
      		<table width = "50%">
				<tr>
					<td>
						<input type="text" name="text">	
					</td>
					<td>
						<input type="button" name="search" value="Search">	
					</td>
				</tr>
			</table>
		</form>
	  </li>
      <li><a href="#one" data-toggle="tab">Eco Fashion</a></li>
      <li><a href="#two" data-toggle="tab">Green Beauty</a></li>
      <li><a href="#three" data-toggle="tab">Green Lifestyle</a></li>
      <li><a href="#four" data-toggle="tab">Sale</a></li>
    </ul>
  </div>
  
  <!-- Sub menu section -->
  <div class="row">
    <div class="tab-content">
      <div class="tab-pane fade in active pull-right" id="search">    
      </div>
      <div class="tab-pane fade pull-right" id="one">
        <div class="navbar navbar-default pull-right submenu">
          <ul class="nav navbar-nav in" id="one-nav">
            <li><a href="#" id="">Tops</a></li>
            <li><a href="#" id="">T-Shirts</a></li>
            <li><a href="#" id="">Bottoms</a></li>
            <li><a href="#" id="">Outware</a></li>
            <li><a href="#" id="">Shoes</a></li>
            <li><a href="#" id="">Jewelry</a></li>
          </ul>
        </div>
      </div>
      <div class="tab-pane fade pull-right" id="two">
        <div class="navbar navbar-default pull-right submenu">
          <ul class="nav navbar-nav in" id="two-nav">
            <li><a href="#" id="">Fragrances</a></li>
            <li><a href="#" id="">Face Wash</a></li>
            <li><a href="#" id="">Essential</a></li>
            <li><a href="#" id="">Body Wash</a></li>
          </ul>
        </div>
      </div>
      <div class="tab-pane fade pull-right" id="three">
        <div class="navbar navbar-default pull-right submenu">
          <ul class="nav navbar-nav in" id="three-nav">
            <li><a href="#" id="">Furniture</a></li>
            <li><a href="#" id="">Kitchen and Dining</a></li>
            <li><a href="#" id="">Bedding</a></li>
            <li><a href="#" id="">Bathroom</a></li>
            <li><a href="#" id="">Food and Drinks</a></li>
          </ul>
        </div>
      </div>
      <div class="tab-pane fade pull-right" id="four">
        <div class="navbar navbar-default pull-right submenu">
          <ul class="nav navbar-nav in" id="four-nav">
            <li><a href="#" id="">Women Casuals</a></li>
            <li><a href="#" id="">Men Casuals</a></li>
            <li><a href="#" id="">Women Formals</a></li>
            <li><a href="#" id="">Men Formals</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!--to display the banners -->
<div>
    <!--main banner with text -->
    <div style="position:relative">
		<img src="mainBanner.jpg"  width="100%" align="middle">
		<div style="position:absolute;top:100px; left:155px; width:800px; height:50px">
		<center><h1><font color="white">A Social Platform That Changes The Way We Shop for Green Lifestyle</font></h1></center>
		</div>
	</div>

    <!--Sub Banners for the main categories  -->	
	<table width="100%">
	<tr>	
	<td align="center">
		<a href="#"><img src="EcoFashionNew.jpg" align="middle" height="60%"></a>
	</td>
	<td align="center">	
		<a href="#"><img src="greenBeauty3.jpg" align="middle" width="80%"></a>
	</td>
	<td align="center">	
		<a href="#"><img src="ecoStyle.jpg" align="middle"></a>
	</td>
	</tr>
	</table>
</div>
</body>
</html>